#! /usr/local/bin/python3

import argparse
import sqlite_bin.varint_decode


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Provides decoding of hex as varint')
    parser.add_argument('the_hex', type=str, nargs='+',
                        help='The hex to decode')
    parser.add_argument('--quiet', type=str,
                        help='Quiet output (no explanation)')

    args = parser.parse_args()

    combined_arguments = "".join(args.the_hex)
    # the_hex = bytearray(args.the_hex.decode("hex"))  # python2 versions
    the_hex = bytes.fromhex(combined_arguments)  # python3 version

    my_varint_decoder = sqlite_bin.varint_decode.varint_decoder()
    result = my_varint_decoder.decode(the_hex)

    print(my_varint_decoder.get_explanation())

