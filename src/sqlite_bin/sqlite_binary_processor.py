
import struct


def get_details(content):

    a = sqlite_parser(content)

    return a.get_details()





class sqlite_parser(object):

    def __init__(self, content):
        self.content = content
        self.header_details = self.__get_sqlite_header()
        self.freelist_pages = self.__get_freelist_pages()


    def get_details(self):
        """Returns the details of the database in a dictionary"""
        temp = {}
        temp['header_details'] = self.header_details
        temp['freelist_pages'] = self.freelist_pages
        return temp


    def __get_sqlite_header(self):
        """Processes the SQLite database header and returns results as dictionary"""
        header = {}
        header['signature'] = self.content[0:16]
        header['page_size'] = struct.unpack(">H", self.content[16:18])[0]
        header['write_version'] = struct.unpack(">B", self.content[18:19])[0]
        header['read_version'] = struct.unpack(">B", self.content[19:20])[0]
        header['reserved_in_page'] = struct.unpack(">B", self.content[20:21])[0]
        header['max_payload_fraction'] = struct.unpack(">B", self.content[21:22])[0]
        header['min_payload_fraction'] = struct.unpack(">B", self.content[22:23])[0]
        header['leaf_payload_fraction'] = struct.unpack(">B", self.content[23:24])[0]
        header['file_change_counter'] = struct.unpack(">I", self.content[24:28])[0]
        header['pages_in_database'] = struct.unpack(">I", self.content[28:32])[0]
        header['first_freelist_trunk_page'] = struct.unpack(">I", self.content[32:36])[0]
        header['total_freelist_pages'] = struct.unpack(">I", self.content[36:40])[0]
        header['schema_cookie'] = struct.unpack(">I", self.content[40:44])[0]
        header['schema_format_no'] = struct.unpack(">I", self.content[44:48])[0]
        header['default_page_cache_size'] = struct.unpack(">I", self.content[48:52])[0]
        header['largest_root_b_tree'] = struct.unpack(">I", self.content[52:56])[0]
        header['text_encoding'] = struct.unpack(">I", self.content[56:60])[0]

        if header['text_encoding'] == 1: header['text_encoding'] = "UTF-8 (1)"
        if header['text_encoding'] == 2: header['text_encoding'] = "UTF-16LE (2)"
        if header['text_encoding'] == 3: header['text_encoding'] = "UTF-16BE (3)"

        header['user_version'] = struct.unpack(">I", self.content[60:64])[0]
        header['incremental_vacuum'] = struct.unpack(">I", self.content[64:68])[0]
        header['app_id'] = struct.unpack(">I", self.content[68:72])[0]
        #header['reserved'] = content[72:92]
        header['version_valid_for'] = struct.unpack(">I", self.content[92:96])[0]
        header['sqlite_version_number'] = struct.unpack(">I", self.content[96:100])[0]

        # determine autovacuum settings...
        if header['largest_root_b_tree'] > 0:
            # is either autovacuum, or incremental vacuum
            if header['incremental_vacuum'] == 1:
                header['vacuum mode (interpreted)'] = 'Incremental Vacuum'
            else:
                header['vacuum mode (interpreted)'] = 'Auto Vacuum'
        else:
            header['vacuum mode (interpreted)'] = 'No Vacuuming'

        return header


    def __get_freelist_pages(self):
        """Returns the indexes of freelist pages """

        page_size = struct.unpack(">H", self.content[16:18])[0]
        first_freelist_trunk_page = struct.unpack(">I", self.content[32:36])[0]

        freelist_pages = []

        # temp_freelist_page = {}
        # temp_freelist_page['page_id'] = 1
        # temp_freelist_page['offset'] = 12335
        # temp_freelist_page['size'] = page_size
        # freelist_pages.append(temp_freelist_page)

        return freelist_pages


    def __process_freelist(self, start):

        page_size = struct.unpack(">H", self.content[16:18])[0]



