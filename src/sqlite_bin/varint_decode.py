#
#  Decodes data as varints.
#
# There are definitly better implementations e.g.
# https://github.com/dowski/misc/blob/master/varints.py
#
# but this one is designed to expose all parts of the process
# for teaching purposes.
#


class varint_decoder(object):

    def __init__(self):
        """Constructor for VARINT decoder"""
        self.data = None
        self.value = None
        self.explanation = ""

    def decode(self, data):
        """Decodes the provided byte array"""
        self.data = data
        try:
            self.value = self.__decode_data_as_int()
            self.__set_explanation()
        except VarIntNotEnoughBytesException:
            self.__set_explanation("Not a VARINT (Not enough bytes)")
        except VarIntTooManyBytesException:
            self.__set_explanation("Not a VARINT (Unexpected bytes)")

        return self.value

    def get_explanation(self):
        """Returns explanation of how the decoding was performed"""
        return self.explanation


    def __set_explanation(self, message=""):
        if self.value != None:
            output = ""
            output += self.original_bytes_string + '\t\t(original bytes)' + '\n'
            output += self.original_bit_string + '\t\t(original bits)' + '\n'
            output += self.masked_bit_string + '\t\t(high order bit masked)' + '\n'
            output += self.__get_masked_bit_string_shifted() + '\t\t(masked bits right shifted)' + '\n'
            output += self.__get_total_as_bin_string() + '\t\t(final bit string)' + '\n'
            output += "{}".format(self.value).rjust(len(self.__get_total_as_bin_string())) + '\t\t(decimal value)' + '\n'
            self.explanation = output
        else:
            self.explanation = message


    def __decode_data_as_int(self):
        """Does the decoding of the bytes and returns int"""
        self.original_bytes_string = ""
        self.original_bit_string = ""
        self.masked_bit_string = ""
        self.shifted_bit_string = ""

        no_bytes = self.__count_no_bytes()


        total = 0
        i = 0
        while True:
            each_byte = self.data[i]

            self.original_bit_string += "{0:08b} ".format(each_byte)
            self.original_bytes_string += "      %02X " % each_byte

            byte_masked = each_byte & 0b01111111
            self.masked_bit_string += " {0:07b} ".format(byte_masked)
            self.shifted_bit_string += "{0:07b}".format(byte_masked)

            if self.__is_high_bit_set(each_byte):
                shift_value = no_bytes - i - 1
                value = byte_masked << (7 * shift_value)
                total += value
            else:
                value = byte_masked
                total += value
                break
            i += 1

        return total

    def __count_no_bytes(self):
        """Counts number of bytes in byte array and checks VARINT correctness"""
        i = 0
        no_bytes = 0
        while True:
            if i >= len(self.data):
                raise VarIntNotEnoughBytesException("Not a VARINT (Not enough bytes)")
            no_bytes += 1
            if not self.__is_high_bit_set(self.data[i]):
                if i < len(self.data)-1:
                    raise VarIntTooManyBytesException('Unexpected bytes')
                break
            i += 1
        return no_bytes

    def __is_high_bit_set(self, byte):
        """Checks high order bit is set"""
        mask = 0b10000000
        if mask & byte == 0b10000000:
            return True
        else:
            return False

    def __get_masked_bit_string_shifted(self):
        """Returns binary string after shifting"""
        self.shifted_bit_string = self.format_shifted_string(self.shifted_bit_string, self.__count_no_bytes())
        no_spaces_to_add_for_masked = self.__count_no_bytes()
        no_spaces_to_add = no_spaces_to_add_for_masked
        out = " " * no_spaces_to_add + self.shifted_bit_string
        return out


    def __get_total_as_bin_string(self):
        """Returns final binary string formatted"""
        final_bit_string = self.__count_no_bytes() * '0' + self.shifted_bit_string
        return final_bit_string

    def format_shifted_string(self, the_string, no_bytes):
        """Formats the bit shifted string with spaces in the correct place"""
        out_str = ''
        index = 0
        if no_bytes > 1:
            for i in range(0, no_bytes):
                index = len(the_string) - 8 * i
                out_str = ' ' + the_string[index-8:index] + out_str
            out_str = the_string[:index] + ' ' + out_str.lstrip()
        else:
            out_str = the_string
        return out_str

class VarIntTooManyBytesException(ValueError):
    pass

class VarIntNotEnoughBytesException(ValueError):
    pass

