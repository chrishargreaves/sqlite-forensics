__author__ = 'chris'

import struct
import re

def __get_live_cells(page_content, no_cells):
    bytes_in_cell_array = no_cells * 2

    live_cells = []
    for i in range(0, bytes_in_cell_array, 2):
        offset_of_cell_pointer = 8 + i
        cell_pointer_data = page_content[offset_of_cell_pointer:offset_of_cell_pointer+2]
        #print(len(cell_pointer_data))
        offset_of_cell = struct.unpack(">H", cell_pointer_data)[0]
        cell = {}
        cell['offset'] = offset_of_cell
        #print(cell)
        live_cells.append(cell)

    return live_cells


def __get_non_zero_content(data):
    count = 0
    for each in data:
        if each != 0:
            count+=1
    return count


def parse_page(content):

    details = {}
    details['offset_to_first_free_block'] = struct.unpack(">H", content[1:3])[0]
    details['no_cells'] = struct.unpack(">H", content[3:5])[0]
    details['offset_to_cell_content_area'] = struct.unpack(">H", content[5:7])[0]
    details['fragmented_bytes_in_free_content_area'] = struct.unpack("B", content[7:8])[0]

    # work out free before live cells
    details['unused_before_cells'] = {}
    end_of_cell_array = (details['no_cells'] * 2) + 8
    details['unused_before_cells']['offset'] = end_of_cell_array
    details['unused_before_cells']['length'] = details['offset_to_cell_content_area'] - end_of_cell_array

    # do live cells
    details['live_cells'] = __get_live_cells(content, details['no_cells'])

    if len(details['live_cells']) > 0:
        details['offset_to_earliest_cell'] = min(details['live_cells'], key=lambda x: x['offset'])['offset']
    else:
        details['offset_to_earliest_cell'] = None

    pre_cell_content = content[details['unused_before_cells']['offset']:
                                details['unused_before_cells']['offset'] + details['unused_before_cells']['length']]

    details['non_zero_bytes_in_unallocated'] = __get_non_zero_content(pre_cell_content)

    # is there any free space between cells
    # process all live cells
    # need varints for this
    # work out length for each
    # any gaps/slack?

    return details

