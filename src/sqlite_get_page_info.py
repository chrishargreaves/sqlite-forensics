#! /usr/local/bin/python3

import logging
import argparse
import os
import sys
import math
import sqlite_bin.sqlite_binary_processor
import sqlite_bin.sqlite_binary_page_parser


def __get_page_size(file_pointer):
    """Returns the page size of the database"""
    data = file_pointer.read()
    s = sqlite_bin.sqlite_binary_processor.sqlite_parser(data)
    page_size = s.header_details['page_size']
    logging.debug("Page size: {}".format(page_size))
    file_pointer.seek(0)
    return page_size

def __process_page_data(data, page_number, offset):
    logging.debug('processing page {}'.format(page_number))
    if data[0] == 0x0D: # indicates table leaf
        print('-'*25)
        print("Page: {} @ offset {}".format(page_number, offset))
        page_details = sqlite_bin.sqlite_binary_page_parser.parse_page(data)
        for each in page_details:
            print("{}: {}".format(each, page_details[each]))
    else:
        logging.debug('Page {} is not a leaf table ({:02X})'.format(page_number, data[0]))
        return None

def __process_all_pages(file_pointer, page_size):
    """Process all pages and print report"""
    file_pointer.seek(0)
    page_number = 0
    offset = 0
    while True:
        data = file_pointer.read(page_size)
        if len(data) < page_size:
            break

        __process_page_data(data, page_number, offset)

        page_number += 1
        offset += page_size


def __process_page(file_pointer, page_size, page_number):
    offset = page_size*page_number
    file_pointer.seek(offset)
    data = file_pointer.read(page_size)
    __process_page_data(data, page_number, offset)

def __process_page_with_offset(file_pointer, page_size, offset):
    page = math.floor(offset / page_size)
    __process_page(file_pointer, page_size, page)


if __name__ == "__main__":

    logging.basicConfig(level=logging.WARNING)
    parser = argparse.ArgumentParser(description='Displays information about a page(s) in a database.')
    parser.add_argument('path', type=str,
                        help='path to SQLite database')
    parser.add_argument('--offset', type=int,
                        help='byte offset into database to get information')
    parser.add_argument('--page', type=int,
                        help='page number to get information')

    args = parser.parse_args()

    if not os.path.exists(args.path):
        print("Path: {} not found".format(args.path))
        sys.exit(-1)

    file_pointer = open(args.path, 'rb')
    page_size = __get_page_size(file_pointer)

    if args.page:
        __process_page(file_pointer, page_size, args.page)
    elif args.offset:
        __process_page_with_offset(file_pointer, page_size, args.offset)
        pass

    elif not args.offset and not args.page:
        __process_all_pages(file_pointer, page_size)
    else:
        print('unknown options')
        sys.exit(-1)

