from distutils.core import setup
import py2exe

# Use with py2exe to create Windows build
# chance to src directory and run:
# python3.exe .\setup_varint.py py2exe
#
# Exe will be built in ../dist directory.


setup(
    windows=[{'script':'decode_varint_gui.py',
              'icon_resources': [(1,'varint.ico')]}],
    options={
        "py2exe":{
            "bundle_files": 2,
            "dist_dir": "../dist"
        }
    }
)

# known simple good
# setup(windows=['src/decode_varint_gui.py'])
