#! /usr/local/bin/python3

import argparse
import os
import sys
import logging
import sqlite_bin.sqlite_binary_processor
import sqlite_bin.sqlite_binary_page_parser


def __get_page_size(file_pointer):
    """Returns the page size of the database"""
    data = file_pointer.read()
    s = sqlite_bin.sqlite_binary_processor.sqlite_parser(data)
    page_size = s.header_details['page_size']
    logging.debug("Page size: {}".format(page_size))
    file_pointer.seek(0)
    return page_size


def __process_pages_unallocated(file_pointer, page_size):
    """ Process all the pages and print those with non-zero in unallocated"""
    file_pointer.seek(0)
    page_number = 0
    offset = 0

    results = []

    while True:
        data = file_pointer.read(page_size)
        if len(data) < page_size:
            break

        logging.debug('processing page {}'.format(page_number))
        if data[0] == 0x0D: # indicates table leaf
            page_details = sqlite_bin.sqlite_binary_page_parser.parse_page(data)
            logging.debug(page_details['non_zero_bytes_in_unallocated'])

            the_result = {'has_unallocated': False,
                          'has_freecells': False}

            if page_details['non_zero_bytes_in_unallocated'] > 0:
                the_result.update({'page_number': page_number,
                                   'page_offset': offset,
                                   'has_unallocated': True,
                                   'non_zero_bytes': page_details['non_zero_bytes_in_unallocated'],
                                   'unallocated_start': offset + page_details['unused_before_cells']['offset'],
                                   'unallocated_end': offset + page_details['unused_before_cells']['offset'] +
                                                  page_details['unused_before_cells']['length'],
                                   'unallocated_data': data[page_details['unused_before_cells']['offset']:
                                                  page_details['unused_before_cells']['offset'] +
                                                    page_details['unused_before_cells']['length']]
                            })

            if page_details['offset_to_first_free_block'] > 0:

                # TODO follow list of free cells

                the_result.update({'page_number': page_number,
                                   'page_offset': offset,
                                   'has_freecells': True,
                                   'offset_to_first_free_block': page_details['offset_to_first_free_block'],
                                   'fragmented_bytes_in_free_content_area': page_details['fragmented_bytes_in_free_content_area']
                            })

            if the_result['has_freecells'] == True or the_result['has_unallocated'] == True:
                results.append(the_result)

        page_number += 1
        offset += page_size

    return results



if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO)

    help_string = 'Output has 4 columns, e.g.\n' \
                  '1	32768	2	31611\n' \
                  '9	294912	3	-\n' \
                  '15	491520	3	-\n' \
                  '17	557056	-	32381\n' \
                  '\n' \
                  'Column 1: Page number\n' \
                  'Column 2: Page offset\n' \
                  'Column 3: Number of non-zero bytes in unallocated space before cell area\n' \
                  'Column 4: Offset to first free block within cell area\n'

    parser = argparse.ArgumentParser(
        description='Check if database has data in table leaves that is unallocated',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=help_string)
    parser.add_argument('path', type=str,
                        help='path to SQLite database')
    parser.add_argument('--export', type=str,
                        help='path to export data from unallocated cells')

    args = parser.parse_args()

    if not os.path.exists(args.path):
        print("Path: {} not found".format(args.path))
        sys.exit(-1)

    file_pointer = open(args.path, 'rb')

    page_size = __get_page_size(file_pointer)

    results = __process_pages_unallocated(file_pointer, page_size)

    print('page\toffset\tnon-zero_in_unallocated\toffset_to_free_cell') # column headings

    for each in results:

        out_str = '{}\t{}\t'.format(each['page_number'], each['page_offset'])

        if each['has_unallocated'] is True:
            out_str += "{}\t".format(each['non_zero_bytes'])
        else:
            out_str += "{}\t".format("-")

        if each['has_freecells'] is True:
            out_str += "{}\t".format(each['offset_to_first_free_block'])
        else:
            out_str += "{}\t".format('-')

        print(out_str)

        if each['has_unallocated'] and args.export:
            filename = "page_{}.bin".format(each['page_number'])
            outfile = open(os.path.join(args.export, filename), 'wb')
            outfile.write(each['unallocated_data'])
            outfile.close()

