import argparse
import sqlite_bin.varint_decode
from tkinter import *
from tkinter import ttk


def decode_hex(*args):
    my_decoder = sqlite_bin.varint_decode.varint_decoder()
    try:
        the_hex = bytes.fromhex(hex_values.get())
    except ValueError:
        decoding_values.set("Input error\n\n\n\n\n\n") # This is very bad! Need to figure out fixed frame size in Tk
        return

    res = my_decoder.decode(the_hex)

    decoding_values.set(my_decoder.get_explanation() + "\n\n\n\n\n\n")


def rClicker(e):
    """ right click context menu for all Tk Entry and Text widgets"""
    # http://stackoverflow.com/questions/4266566/stardand-context-menu-in-python-tkinter-text-widget-when-mouse-right-button-is-p

    try:
        def rClick_Copy(e, apnd=0):
            e.widget.event_generate('<Control-c>')

        def rClick_Cut(e):
            e.widget.event_generate('<Control-x>')

        def rClick_Paste(e):
            e.widget.event_generate('<Control-v>')

        e.widget.focus()

        nclst=[
               (' Cut', lambda e=e: rClick_Cut(e)),
               (' Copy', lambda e=e: rClick_Copy(e)),
               (' Paste', lambda e=e: rClick_Paste(e)),
               ]

        rmenu = Menu(None, tearoff=0, takefocus=0)

        for (txt, cmd) in nclst:
            rmenu.add_command(label=txt, command=cmd)

        rmenu.tk_popup(e.x_root+40, e.y_root+10,entry="0")

    except TclError:
        print(' - rClick menu, something wrong')
        pass

    return "break"


def rClickbinder(r):
    try:
        for b in [ 'Text', 'Entry', 'Listbox', 'Label']: #
            r.bind_class(b, sequence='<Button-3>',
                         func=rClicker, add='')
    except TclError:
        print(' - rClickbinder, something wrong')
        pass


if __name__ == "__main__":

    root = Tk()
    root.title("Convert VARINTS")

    mainframe = ttk.Frame(root, width=70, height=70, padding="3 2 12 12")
    mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
    mainframe.columnconfigure(0, weight=1)
    mainframe.rowconfigure(0, weight=1)

    hex_values = StringVar()
    decoding_values = StringVar()

    decoding_values.set("\n\n\n\n\n\n")

    hex_entry = ttk.Entry(mainframe, width=50, textvariable=hex_values)
    hex_entry.grid(column=2, row=0, sticky=(W, E))

    ttk.Label(mainframe, textvariable=decoding_values, font="Courier").grid(column=2, row=1, sticky=(W, E))
    ttk.Button(mainframe, text="Calculate", command=decode_hex).grid(column=3, row=0, sticky=W)

    ttk.Label(mainframe, text="Hex:").grid(column=0, row=0, sticky=W)
    ttk.Label(mainframe, text="Decoding:").grid(column=0, row=1, sticky=E)

    for child in mainframe.winfo_children():
        child.grid_configure(padx=5, pady=5)

    hex_entry.focus()
    root.bind('<Return>', decode_hex)

    rClickbinder(root)

    root.mainloop()



    #
    # # the_hex = bytearray(args.the_hex.decode("hex"))  # python2 versions
    # the_hex = bytes.fromhex(args.the_hex)  # python3 version
    #
    # my_varint_decoder = sqlite_bin.varint_decode.varint_decoder()
    # result = my_varint_decoder.decode(the_hex)
    #
    # if result:
    #     print(my_varint_decoder.get_explanation())
    # else:
    #     print("Not a VARINT.")
    #
    # # print(decode_hex(args.the_hex))


# This is all added in just to support copy and paste!





