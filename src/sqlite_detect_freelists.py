#! /usr/local/bin/python3

import argparse
import os
import sys
import sqlite_bin.sqlite_binary_processor


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Check if database has free pages')
    parser.add_argument('path', type=str,
                        help='path to SQLite database')
    args = parser.parse_args()

    if not os.path.exists(args.path):
        print("Path: {} not found".format(args.path))
        sys.exit(-1)

    filename = args.path

    f = open(filename, 'rb')
    data = f.read()
    s = sqlite_bin.sqlite_binary_processor.sqlite_parser(data)
    if s.header_details['total_freelist_pages'] > 0:
        print("{}\t{}".format(filename, s.header_details['total_freelist_pages']))
