# README #

### Intro ###

* A collection of tools to help with SQLite database binary analysis

### Current Tools ###

* decode_varint - convert hex string as a varint
* decode_varint_gui - gui version of hex to varint conversion
* sqlite_detect_freelists - point at a database and reports if freelists are present
* sqlite_detect_unallocated_cells - use to detect and dump unallocated areas of table leaf pages in a database
* sqlite_get_page_info - displays information about a database page

### Tool Use Examples: decode_varint ###

```
 usage: decode_varint [-h] [--quiet QUIET] the_hex [the_hex ...]
```

e.g.

```
$ python3 decode_varint.py 7F
      7F 		(original bytes)
01111111 		(original bits)
 1111111 		(high order bit masked)
 1111111		(masked bits right shifted)
01111111		(final bit string)
     127		(decimal value)
```

```
$ python3 decode_varint.py 81 00
      81       00 		(original bytes)
10000001 00000000 		(original bits)
 0000001  0000000 		(high order bit masked)
  000000 10000000		(masked bits right shifted)
00000000 10000000		(final bit string)
              128		(decimal value)
```

Spaces in hex string input are optional

```
$ python3 decode_varint.py 8100
      81       00 		(original bytes)
10000001 00000000 		(original bits)
 0000001  0000000 		(high order bit masked)
  000000 10000000		(masked bits right shifted)
00000000 10000000		(final bit string)
              128		(decimal value)
```

For a GUI based version use ```python3 decode_varint_gui.py```

Windows binary of GUI version available in Downloads section. 

### Tool Use Examples: sqlite_get_page_info ###

Basic usage will dump information about all leaf table pages:

e.g. 

```
$ sqlite_get_page_info places.sqlite 
-------------------------
Page: 1 @ offset 32768
fragmented_bytes_in_free_content_area: 1
offset_to_first_free_block: 31611
no_cells: 17
unused_before_cells: {'offset': 42, 'length': 31318}
offset_to_earliest_cell: 31360
non_zero_bytes_in_unallocated: 2
live_cells: [{'offset': 31991}, {'offset': 32681}, {'offset': 31898}, {'offset': 32595}, {'offset': 32085}, {'offset': 32372}, {'offset': 32226}, {'offset': 32166}, {'offset': 32492}, {'offset': 31776}, {'offset': 31703}, {'offset': 32447}, {'offset': 31560}, {'offset': 31515}, {'offset': 31469}, {'offset': 31416}, {'offset': 31360}]
offset_to_cell_content_area: 31360
-------------------------
Page: 9 @ offset 294912
fragmented_bytes_in_free_content_area: 0
offset_to_first_free_block: 0
no_cells: 3
unused_before_cells: {'offset': 14, 'length': 32698}
offset_to_earliest_cell: 32712
non_zero_bytes_in_unallocated: 3
live_cells: [{'offset': 32750}, {'offset': 32731}, {'offset': 32712}]
offset_to_cell_content_area: 32712

...[output truncated]...
```

The part of the output that can be helpful is the count of non-zero bytes in the unallocated area (before cell content area) of the page

Specific pages can be queried using the --page option:

```
$ sqlite_get_page_info --page 9 places.sqlite 
-------------------------
Page: 9 @ offset 294912
offset_to_first_free_block: 0
no_cells: 3
unused_before_cells: {'length': 32698, 'offset': 14}
offset_to_earliest_cell: 32712
fragmented_bytes_in_free_content_area: 0
live_cells: [{'offset': 32750}, {'offset': 32731}, {'offset': 32712}]
non_zero_bytes_in_unallocated: 3
offset_to_cell_content_area: 32712
```

Also an offset into the database can be supplied. This is useful if you have a keyword hit and want to dump the information about the page it contains.

e.g. a url bbc new

```
Offset      0  1  2  3  4  5  6  7   8  9 10 11 12 13 14 15

00064432   00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00                   
00064448   00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00                   
00064464   00 00 00 00 00 00 00 47  0B 0C 00 39 00 2B 09 09          G   9 +  
00064480   09 00 02 06 25 68 74 74  70 3A 2F 2F 6E 65 77 73       %http://news
00064496   2E 62 62 63 2E 63 6F 2E  75 6B 2F 6B 75 2E 6F 63   .bbc.co.uk/ku.oc
00064512   2E 63 62 62 2E 73 77 65  6E 2E 07 D0 00 05 08 D9   .cbb.swen. Ð   Ù
00064528   FE A6 2C B0 49 78 65 2D  76 6A 75 58 2D 69 36 49   þ¦,°Ixe-vjuX-i6I
00064544   78 0A 0C 00 75 31 2D 09  08 08 01 01 06 25 68 74   x   u1-      %ht
00064560   74 70 73 3A 2F 2F 77 77  77 2E 6D 6F 7A 69 6C 6C   tps://www.mozill
00064576   61 2E 6F 72 67 2F 65 6E  2D 55 53 2F 66 69 72 65   a.org/en-US/fire
00064592   66 6F 78 2F 33 33 2E 30  2F 66 69 72 73 74 72 75   fox/33.0/firstru
```

We can dump the information about the page using:

```
$ sqlite_get_page_info --offset 64480 places.sqlite 
-------------------------
Page: 1 @ offset 32768
offset_to_first_free_block: 31611
no_cells: 17
live_cells: [{'offset': 31991}, {'offset': 32681}, {'offset': 31898}, {'offset': 32595}, {'offset': 32085}, {'offset': 32372}, {'offset': 32226}, {'offset': 32166}, {'offset': 32492}, {'offset': 31776}, {'offset': 31703}, {'offset': 32447}, {'offset': 31560}, {'offset': 31515}, {'offset': 31469}, {'offset': 31416}, {'offset': 31360}]
non_zero_bytes_in_unallocated: 2
offset_to_cell_content_area: 31360
fragmented_bytes_in_free_content_area: 1
offset_to_earliest_cell: 31360
unused_before_cells: {'length': 31318, 'offset': 42}
```


### Tool Use Examples: sqlite_detect_unallocated_cells ###

Detects and displays database table leaf pages that have data in unallocated areas. 
Includes:

* Data in unallocated before the cell area of the page
* Data in free cells within cell area (some detection but still in progress) 

Output has four columns, e.g.

```
1	32768	2	31611
9	294912	3	-
15	491520	3	-
17	557056	-	32381
```


Column 1: Page number
Column 2: Page offset
Column 3: Number of non-zero bytes in unallocated space before cell area
Column 4: Offset to first free block within cell area


### Tool Use Examples: sqlite_detect_freelists ###

* Todo




