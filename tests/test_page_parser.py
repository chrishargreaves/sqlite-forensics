import unittest
import sqlite_bin.sqlite_binary_page_parser

class MyTestCase(unittest.TestCase):

    def test_page_1(self):

        f = open('page_test_data/test_page1.bin', 'rb')
        data = f.read()
        results = sqlite_bin.sqlite_binary_page_parser.parse_page(data)

        print(results)

        self.assertEqual(results['offset_to_first_free_block'], 31611)
        self.assertEqual(results['no_cells'], 17)
        self.assertEqual(results['offset_to_cell_content_area'], 31360)
        self.assertEqual(results['fragmented_bytes_in_free_content_area'], 1)
        self.assertEqual(results['unused_before_cells'], {'offset': 42, 'length': 31318})
        self.assertEqual(results['non_zero_bytes_in_unallocated'], 2)


if __name__ == '__main__':
    unittest.main()
