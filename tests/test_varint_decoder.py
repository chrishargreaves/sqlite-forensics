from sqlite_bin import varint_decode

__author__ = 'chris'

import unittest
import re

class MyTestCase(unittest.TestCase):

    # def test01_explanation(self):
    #     my_decoder = varint_decode.varint_decoder()
    #     res = my_decoder.decode(b"\x01")
    #     explanation = my_decoder.get_explanation()
    #     self.assertEqual(True, explanation.find("(original bytes)"))
    #     # self.assertEqual(True, re.search("00000001.*(original bits)", explanation))
    #     # self.assertEqual(True, re.search("0000001.*(high order bit masked)", explanation))
    #     # self.assertEqual(True, re.search(" 0000001.*(masked bits right shifted)", explanation))
    #     # self.assertEqual(True, re.search("00000001.*(final bit string)", explanation))
    #     # self.assertEqual(True, re.search("       1.*(decimal value)", explanation))

    def test_17_returns_correct_value(self):
        my_decoder = varint_decode.varint_decoder()
        res = my_decoder.decode(b"\x17")
        my_decoder.get_explanation()
        self.assertEqual(res, 23)
        print(my_decoder.get_explanation())

    def test_11_returns_correct_value(self):
        my_decoder = varint_decode.varint_decoder()
        res = my_decoder.decode(b"\x11")
        my_decoder.get_explanation()
        self.assertEqual(res, 17)
        print(my_decoder.get_explanation())

    def test_8101_returns_correct_value(self):
        my_decoder = varint_decode.varint_decoder()
        res = my_decoder.decode(b"\x81\x01")
        my_decoder.get_explanation()
        self.assertEqual(res, 129)
        print(my_decoder.get_explanation())

    def test_b210_returns_correct_value(self):
        my_decoder = varint_decode.varint_decoder()
        res = my_decoder.decode(b"\xb2\x10")
        my_decoder.get_explanation()
        self.assertEqual(res, 6416)
        print(my_decoder.get_explanation())

    def test_45_returns_correct_value(self):
        my_decoder = varint_decode.varint_decoder()
        res = my_decoder.decode(b"\x45")
        my_decoder.get_explanation()
        self.assertEqual(res, 69)
        print(my_decoder.get_explanation())

    def test_819502_returns_correct_value(self):
        my_decoder = varint_decode.varint_decoder()
        res = my_decoder.decode(b"\x81\x95\x02")
        my_decoder.get_explanation()
        self.assertEqual(res, 19074)
        print(my_decoder.get_explanation())

    def test_15_returns_correct_value(self):
        my_decoder = varint_decode.varint_decoder()
        res = my_decoder.decode(b"\x15")
        my_decoder.get_explanation()
        self.assertEqual(res, 21)
        print(my_decoder.get_explanation())

    def test_79_returns_correct_value(self):
        my_decoder = varint_decode.varint_decoder()
        res = my_decoder.decode(b"\x79")
        my_decoder.get_explanation()
        self.assertEqual(res, 121)
        print(my_decoder.get_explanation())

    def test_80_returns_none(self):
        my_decoder = varint_decode.varint_decoder()
        res = my_decoder.decode(b"\x80")
        self.assertEqual(res, None)


    def test_explanation_with_no_value(self):
        my_decoder = varint_decode.varint_decoder()
        res = my_decoder.decode(b"\x80")
        self.assertEqual("Not a VARINT (Not enough bytes)", my_decoder.get_explanation())

    def test_7901_returns_none(self):
        my_decoder = varint_decode.varint_decoder()
        res = my_decoder.decode(b"\x79\x01")
        self.assertEqual(res, None)

    def test_explanation_with_too_many_bytes(self):
        my_decoder = varint_decode.varint_decoder()
        res = my_decoder.decode(b"\x79\x01")
        self.assertEqual("Not a VARINT (Unexpected bytes)", my_decoder.get_explanation())

    def test_explanation_of_strange_3_byte_case(self):
        my_decoder = varint_decode.varint_decoder()
        res = my_decoder.decode(b"\x80\x80\x01")
        my_decoder.get_explanation()
        self.assertEqual(res, 1)
        print(my_decoder.get_explanation())

    def test_formatting1(self):
        my_decoder = varint_decode.varint_decoder()
        res = my_decoder.decode(b"\x01")
        res = my_decoder.format_shifted_string('0000001', 1)
        self.assertEqual('0000001', res)

    def test_formatting2(self):
        my_decoder = varint_decode.varint_decoder()
        res = my_decoder.decode(b"\x81\x01")
        res = my_decoder.format_shifted_string('00000010000001', 2)
        self.assertEqual('000000 10000001', res)

    def test_formatting3(self):
        my_decoder = varint_decode.varint_decoder()
        res = my_decoder.decode(b"\x81\x81\x01")
        res = my_decoder.format_shifted_string('000000100000010000001', 3)
        self.assertEqual('00000 01000000 10000001', res)


if __name__ == '__main__':
    unittest.main()
